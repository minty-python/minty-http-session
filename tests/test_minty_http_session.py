import codecs
import time

import pytest

from minty_http_session.retriever import (
    SessionDataNotFoundError,
    SessionNotFoundError,
    SessionRetriever,
)


class TestSessionReader:
    """Test the session reader"""

    def test_retrieve(self):
        mock_redis = {
            "json:session:31337": codecs.encode(
                b'{"username":"piet"}', "base64"
            ),
            "expires:31337": str(time.time() + 3600),
        }
        retriever = SessionRetriever(mock_redis)

        session_data = retriever.retrieve("31337")

        assert session_data["username"] == "piet"

    def test_expired_session(self):
        mock_redis = {
            "json:session:31337": codecs.encode(
                b'{"username":"piet"}', "base64"
            ),
            "expires:31337": str(time.time() - 3600),
        }
        retriever = SessionRetriever(mock_redis)

        with pytest.raises(SessionNotFoundError):
            retriever.retrieve("31337")

    def test_nonexistent_session(self):
        mock_redis = {
            "json:session:31337": codecs.encode(
                b'{"username":"piet"}', "base64"
            ),
            "expires:31337": str(time.time() - 3600),
        }
        retriever = SessionRetriever(mock_redis)

        with pytest.raises(SessionNotFoundError):
            retriever.retrieve("31338")

    def test_empty_session(self):
        mock_redis = {
            "json:session:31337": None,
            "expires:31337": str(time.time() + 3600),
        }
        retriever = SessionRetriever(mock_redis)

        with pytest.raises(SessionDataNotFoundError):
            retriever.retrieve("31337")
