import codecs
import json
import time

from redis import StrictRedis


class SessionRetrieverError(Exception):
    """Base for session retrieval related exceptions"""

    pass


class SessionNotFoundError(SessionRetrieverError, ValueError):
    """Session can't be found in Redis"""

    pass


class SessionDataNotFoundError(SessionRetrieverError, ValueError):
    """Session data can't be found in Redis"""

    pass


class SessionRetriever:
    """HTTP session retriever

    Retrieves a session stored by the Perl application from the Redis store
    """

    __slots__ = ["redis", "decoder"]

    def __init__(self, redis: StrictRedis):
        """Initialize a HTTP session retriever

        :param redis: Redis connection object to retrieve session data
        :type redis: StrictRedis
        """

        self.redis = redis

    def retrieve(self, session_id: str) -> dict:
        """Retrieve a session from Redis

        :param session_id: session id to retrieve
        :type session_id: str
        :raises SessionNotFoundError: if the session cannot be found
        :raises SessionDataNotFoundError: if the session is found, but contains
                                          no data
        :return: the session data
        :rtype: dict
        """

        now = time.time()
        expiration = self.redis.get(f"expires:{session_id}")

        if expiration is None:
            raise SessionNotFoundError(session_id)

        if float(expiration) < now:
            raise SessionNotFoundError(session_id)

        session_data_raw = self.redis.get(f"json:session:{session_id}")

        if session_data_raw is None:
            raise SessionDataNotFoundError(session_id)

        session_data_decoded = codecs.decode(session_data_raw, "base64")
        session_data = json.loads(session_data_decoded)

        return session_data
